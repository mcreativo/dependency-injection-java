public class ClassA
{
    private ClassB classB;

    private String one;

    private String two;

    public ClassA(ClassB classB, String one, String two)
    {
        this.classB = classB;
        this.one = one;
        this.two = two;
    }
}
